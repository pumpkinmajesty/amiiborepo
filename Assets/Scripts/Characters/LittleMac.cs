﻿using UnityEngine;
using System.Collections;

public class LittleMac : BoardSquare {

	protected override void Awake () {
		base.Awake();
		images[1] = Resources.Load ("littlemac") as Texture2D;

		/*
		 * set timeBonus value
		 * 
		 */

	}
	
	protected override void Start() {
		base.Start ();
		
		/*
		 * set the value in the text box to display
		 * 
		 */
		textBox.GetComponent<TextMesh>().text = "LittleMac";
	}
	
	protected override void onSquareSelected() {
		base.onSquareSelected();
	}	
	
	protected override void updateEndGraphic ()
	{
		base.updateEndGraphic();
	}
	
}